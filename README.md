# Eye-tracker

[![pipeline status](https://gitlab.com/jakubgruber/eye-tracker/badges/master/pipeline.svg)](https://gitlab.com/jakubgruber/eye-tracker/commits/master)[![coverage report](https://gitlab.com/jakubgruber/eye-tracker/badges/master/coverage.svg)](https://gitlab.com/jakubgruber/eye-tracker/commits/master)[![Release](https://jitpack.io/v/com.gitlab.jakubgruber/eye-tracker.svg)]
(https://jitpack.io/#com.gitlab.jakubgruber/eye-tracker)

---

Eye-tracker is an Android library which aims to simplify eye tracking in Android applications
and therefore make it more used.

It wraps [Mobile Vision API](https://developers.google.com/vision/) and allows to design
the app in the way that it can react to user's eye and face reactions.

Framework also stores data about tracking which can be later evaluated using machine learning
algorithm. However, it is something developer needs to do on his own.

Furthermore, Eye-tracker communicates with [AWS Rekognition Service](https://aws.amazon.com/rekognition/)
and is able to authenticate user based on his photo.

# Installation

Framework was tested using following software:
- Windows 10 x86
- Java 8 (Oracle - 1.8.0_162)
- Android Studio 3.1
- Gradle 4.4
- Android Software Development Kit *(should be downloaded with Android Studio)*

It is recommended to checkout the project and use `gradlew` instead of custom Gradle installation.

Framework is written in Kotlin, but it should not require any external dependencies to be used.
If you want to see framework sources, it is recommended to install `Kotlin Plugin (1.2.31)` to Android Studio.

## Setting framework dependency

To include framework in your project, open root `build.gradle` of the project and register [Jitpack.io](https://jitpack.io/)
repository.

```groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```

In your application module `build.gradle`, add:

```groovy
dependencies {
    implementation 'com.gitlab.jakubgruber:eye-tracker:0.1'
}
```

To get the latest version of the framework, go to [Jitpack.io](https://jitpack.io) and search for
`com.gitlab.jakubgruber/eye-tracker`.
Another option would be to follow recommendations from Android Studio which is able to scan
dependencies and usually offers new versions.

# Project structure

Project consists of following modules:

- **eyetracker** is the eyetracking library itself. It contains classes that handle camera,
 subsribers and frame processing.
- **sample** contains demonstration of how to use eyetracker library module.
- **sample-dynamic-authentication** shows how can be the framework used for dynamic authentication of user.
It logs user using Facebook and Firebase authentication and once login is successful, it check in specified
intervals if user in front of camera did not change.

- **sample-with-preview** - very basic example which is used to test and check what framework actually
does and how the tracking works. It is a good point to start at.

# Getting started

## Precondition

Developer should have knowledge of Java language and at least basic experience
with Android Development to understand concepts of Activities, Fragments and their
lifecycle.

## Recommendations

### Application architecture

Application should be build using fragments. It means that it should contain one activity
and logic should be performed in attached fragments.

One of the fragments attached would be `EyeTrackerFragment`.

### Kotlin compatibility

I highly recommend to use Kotlin for you application. It is a new language that is officially
supported by Google for Android development and by using it, it is ensured that Eye-Tracker will
work as it should.

Even though compatibility with Java should be 100%, there is a possibility that Java will not
see some methods because of missing `@JvmField` and `@JvmStatic` annotation.

In case you encounter similar problem, please let me know. Preferably create issue on Gitlab.

## Init framework in Application class

Eye-Tracker uses its own database and in order to initialize it, it is necessary to inject
`Context` in the framework.

This should be done either in `Application` class or in the `MainAcitivity`. Method `onCreate`
is the most suitable one for that.

```kotlin
EyeTracker.init(this)
```

## Attach fragment to Activity

Once the framework is initialized, attach `EyeTrackerFragment` which will handle eye tracking.
Fragment can be created using builder pattern as shown below.

```kotlin
eyeTrackerFragment = EyeTrackerFragment.Builder
                    .withFps(20.0f)
                    .withPreview(false)
                    .withPrecision(EyeTrackingLevel.HIGH_PRECISION)
                    .withAwsAccessKeys(
                            PropertyReader.getProperty(PropertyReader.ACCESS_KEY_ID, this),
                            PropertyReader.getProperty(PropertyReader.SECRET_ACCESS_KEY, this))
                    .build()
```

As seen above, builder has a few methods that can be configured.

**Mandatory**

- `withAwsAccessKeys` - accessKeyId and secretAccessKey for AWS Rekognition.
 Is necessary for `registerUser` and `verifyUser` methods.

**Optional**
- `withFps` - allows changing number of frames that are processed each second. Lower number,
lower precission.
- `withPreview` - if set to `true`, preview will be visible. By default it is disabled,
because tracking should be performed on background without disturbing user.
- `withPrecision` - changes which events will be triggered. Possibilities are changed by `EyeTrackingLevel`.

`EyeTrackingLevel.HIGH_PRECISION` - all events are triggered.

`EyeTrackingLevel.MAJOR_EVENTS` - only `@OnFaceAppeared` and `@OnFaceLost` events are triggered.

`EyeTrackingLevel.DO_NOT_DISTURB` - no events are triggered.

## Subscribe for eye tracking events

Single fragments can subscribe to receive events. To start receiving events, fragment has to call

```kotlin
EyeTrackerAnnotationProcessor.inject(this)
```  

where `this` is fragment itself (or Context).

Once fragments ends (`onPause`, `onStop`, `onDestroyView` are called), it disconnect from events.

```kotlin
EyeTrackerAnnotationProcessor.disconnect(this)
```

Now to event processing. All application has to do is to annotate functions of class that injected
self to `EyeTrackerAnnotationProcessor` with one of following annotations:

- `OnContextUpdate` - called with every frame. Not recommended to use too often.
- `OnFaceAppeared` - called when user pays attention to app. Good place to start playing video, etc.
- `OnFaceLost` - called when user loses attention. Recommended to stop video, dim the screen, hide sensitive sections, etc.
- `OnYawnDetected` - called when it was detected that user is yawning. It indicates lower attention.

Example of function implementation below:

```kotlin
@OnFaceAppeared
fun onFaceAppeared(eyeContext: EyeTrackerContext) {
    activity!!.runOnUiThread {
        if (!videoView.isPlaying) {
            videoView.start()
        }
    }
}
```

A few points to note here - function must necessarily be public *(by default in Kotlin)*
and has to accept `EyeTrackerContext` as a parameter *(explained in next section)*. Also do not forget
to call `runOnUiThread` if you want to perform some logic on UI. Function is invoked from a different thread.

### EyeTrackerContext

Class that represents context of Eye-Tracker. It contains a few fields that can be used to adjust
UI adaptation. Its parameters should be self-explanatory.

```kotlin
class EyeTrackerContext {
    var facePresent = false
    var leftEyeOpen = false
    var rightEyeOpen = false
    var blinkDetected = false
    var yawnDetected = false
}
```

## User Attention Tree

Framework allows developers to store and analyze events for eye tracking. In order to do that,
fragment should along with calling `EyeTrackerAnnotationProcessor.inject(this)` implement also
`UserAttentionAwareComponent` interface.

```kotlin
interface UserAttentionAwareComponent {
    fun awareComponent(): AwareComponent
}
```

This interface has a single method `awareComponent` which returns object of type `AwareComponent`.
This class represents section in tree hierarchy.

```kotlin
data class AwareComponent(
        val sectionIdentifier: String,
        val parentSectionIdentifier: String? = null)
```

 - `sectionIdentifier` - unique identifier for given section in application. E.g. `loginScreenFragment`.
 - `parentSectionIdentifier` - parent of this section. Can be null. If is not null, then it should match
 some component that has `sectionIdentifier` equals to this field. Parent section should also `inject` self
 before its child component.

### Structure

`UserAttentionTree` provides information about eye tracking events on particular sections structured
in tree hierarchy.

One section is represented by `UserAttentionNode`. This node contains information about `sectionIdentifier`
and its `parent` and `children`.

Along with that, `UserAttentionNode` has a list of `UserAttentionEvent` objects. These objects represents
every event that was triggered for given node (section).

`UserAttentionEvent` is also linked with `UserAttentionNode` it is associated to.

Classes structure is shown below:

```kotlin
data class UserAttentionTree(var root: UserAttentionNode? = null)
```

```kotlin
data class UserAttentionNode(

        var id: Long = 0,
        var sectionIdentifier: String = "",
        var parentNodeId: Long = 0
) {
    var parent: UserAttentionNode? = null
    var children: List<UserAttentionNode> = emptyList()
    var events: List<UserAttentionEvent> = emptyList()
}
```

```kotlin
data class UserAttentionEvent(
        var id: Long = 0,
        var event: String = "",
        var createdDate: Date = Date(),
        var userAttentionNodeId: Long = 0
) {
    lateinit var node: UserAttentionNode
}
```

### Query the tree

To query the tree, one has to only call `EyeTracker.buildTree` method and pass desired `sectionIdentifier`.
This identifier corresponds to `sectionIdentifier` in `AwareComponent`.

Framework will return `UserAttentionTree` with root in node that is identified by `sectionIdentifier`.

## Make use of dynamic authentication

To use dynamic authentication, `accessKeyId` and `secretAccessKey` must be passed to `EyeTrackerFragment` via
its builder.

> Access keys will be provided on demand.

Sample applications acquire access and secret key by `PropertyReader` class that reads `aws.properties` file
stored in `sample/src/main/res/raw/aws.properties`. Because of security reasons, `aws.properties` file is
not stored in VCS.

To make samples work, request access keys and create `aws.properties` file in `/src/main/res/raw/`.
Properties file needs to have following properties defined:
- aws.access.key.id *(Access key ID to AWS Rekognition)*
- aws.secret.access.key *(Secret access key to AWS Rekognition)*

> Do never store access keys in version control!

### Register user

Framework allows to dynamically authenticate users that are using the application. When user is registering
in the application, `registerUser` method of `EyeTrackerFragment` should be called. It requires
`username` that should unique in the app and also `PictureTakenListener`. Listener is notified
with capture bitmap photo of user (can be displayed to user) and with `TakePictureResult` which
contains information about the result.

- `TakePictureListener.OK` - bitmap will be passed in a result and will be not null. User was registered.
- `TakePictureListener.NULL_USERNAME` - invalid username was passed as an argument. Either null or empty.
No picture was taken
- `TakePictureListener.UNEXPECTED_ERROR` - something unexpected has happened, try again later.

### Verify User

Once user is registered in the framework, it can be verified using framework again. Eye-Tracker captures
photo again and compares the face with the one that was captured in registration process.

Verification can be performed by invoking `verifyUser` method of `EyeTrackerFragment`. It again requires
`username` and in addition also `UserVerificationListener` that is notified with the result.

Result is of type `UserVerificationResult` and can be in one of following states:

- `UserVerificationResult.VERIFIED` - user matches the one that was registered, everything ok.
- `UserVerificationResult.NOT_VERIFIED` - user has changed. Consider logging user out or showing a warning.
- `UserVerificationResult.NO_INFORMATION` - method `registerUser` was not called before.
- `UserVerificationResult.UNEXPECTED_ERROR` - something strange going on, try again later.
