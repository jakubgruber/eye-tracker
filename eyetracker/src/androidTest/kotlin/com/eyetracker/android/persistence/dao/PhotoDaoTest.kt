package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.eyetracker.android.persistence.EyeTrackerDatabase
import com.eyetracker.android.persistence.model.Photo
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PhotoDaoTest {

    private lateinit var photoDao: PhotoDao
    private lateinit var eyeTrackerDatabase: EyeTrackerDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getContext()
        eyeTrackerDatabase = Room.inMemoryDatabaseBuilder(context, EyeTrackerDatabase::class.java).build()
        photoDao = eyeTrackerDatabase.PhotoDao()
    }

    @After
    fun closeDb() {
        eyeTrackerDatabase.close()
    }

    @Test
    fun createPhoto_save_queryFromDb_mustBeSame() {
        val photo = Photo(username = "new_username", path = "/path/to/file")

        photoDao.insert(photo)

        val queriedPhoto = photoDao.findByUsername(photo.username)
        assertNotNull(photo.id)
        assertEquals(photo.username, queriedPhoto?.username)
        assertEquals(photo.path, queriedPhoto?.path)
    }

    @Test
    fun createMultiplePhotos_save_idShouldBeGenerated_andDifferent() {
        val photo = Photo(username = "new_username", path = "/path/to/file")
        val photo2 = Photo(username = "new_username2", path = "/path/to/file2")

        photoDao.insert(photo, photo2)

        val queriedPhoto = photoDao.findByUsername(photo.username)
        val queriedPhoto2 = photoDao.findByUsername(photo2.username)

        assertNotNull(queriedPhoto)
        assertNotNull(queriedPhoto2)
        assertNotEquals(queriedPhoto!!.id, queriedPhoto2!!.id)
    }


}