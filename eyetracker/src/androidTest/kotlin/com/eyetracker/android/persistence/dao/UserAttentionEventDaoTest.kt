package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.eyetracker.android.persistence.EyeTrackerDatabase
import com.eyetracker.android.persistence.model.UserAttentionEvent
import com.eyetracker.android.persistence.model.UserAttentionNode
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class UserAttentionEventDaoTest {

    private lateinit var eyeTrackerDatabase: EyeTrackerDatabase
    private lateinit var userAttentionEventDao: UserAttentionEventDao
    private lateinit var userAttentionNodeDao: UserAttentionNodeDao

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getContext()
        eyeTrackerDatabase = Room.inMemoryDatabaseBuilder(context, EyeTrackerDatabase::class.java).build()
        userAttentionNodeDao = eyeTrackerDatabase.UserAttentionNodeDao()
        userAttentionEventDao = eyeTrackerDatabase.UserAttentionEventDao()
    }

    @After
    fun closeDb() {
        eyeTrackerDatabase.close()
    }

    @Test
    fun createNode_insert_createEvent_linkWithNode_queryBoth() {
        val node = UserAttentionNode(sectionIdentifier = "SectionIdentifier")
        val nodeId = userAttentionNodeDao.insert(node)

        val event = UserAttentionEvent(event = "OnAttentionLost", createdDate = Date(), userAttentionNodeId = nodeId)
        val eventId = userAttentionEventDao.insert(event)

        val nodeWithEvents = userAttentionNodeDao.getNodeWithChildrenAndEventsById(nodeId)

        assertNotNull(nodeWithEvents)
        assertNotNull(nodeWithEvents!!.parent)
        assertNotNull(nodeWithEvents.events)
        assertEquals(1, nodeWithEvents.events.size)
        assertEquals(eventId, nodeWithEvents.events[0].id)
    }

    @Test
    fun createNode_insert_createMultipleEvents_linkWithNode_queryAll() {
        val node = UserAttentionNode(sectionIdentifier = "SectionIdentifier")
        val nodeId = userAttentionNodeDao.insert(node)

        for (i in 0 until 10) {
            val event = UserAttentionEvent(event = "OnAttentionLost", createdDate = Date(), userAttentionNodeId = nodeId)
            userAttentionEventDao.insert(event)
        }

        val nodeWithEvents = userAttentionNodeDao.getNodeWithChildrenAndEventsById(nodeId)

        assertNotNull(nodeWithEvents)
        assertNotNull(nodeWithEvents!!.parent)
        assertNotNull(nodeWithEvents.events)
        assertEquals(10, nodeWithEvents.events.size)
    }

}