package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.eyetracker.android.persistence.EyeTrackerDatabase
import com.eyetracker.android.persistence.model.UserAttentionNode
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserAttentionNodeDaoTest {

    private lateinit var userAttentionNodeDao: UserAttentionNodeDao
    private lateinit var eyeTrackerDatabase: EyeTrackerDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getContext()
        eyeTrackerDatabase = Room.inMemoryDatabaseBuilder(context, EyeTrackerDatabase::class.java).build()
        userAttentionNodeDao = eyeTrackerDatabase.UserAttentionNodeDao()
    }

    @After
    fun closeDb() {
        eyeTrackerDatabase.close()
    }

    @Test
    fun queryUnknownNode_shouldBeNull() {
        val node = userAttentionNodeDao.findBySectionIdentifier("")
        assertNull(node)
    }

    @Test
    fun createNode_insertToDb_queryById_mustNotBeNull() {
        val node = UserAttentionNode(sectionIdentifier = "WillBeQueriedById")
        val nodeId = userAttentionNodeDao.insert(node)

        val queriedNode = userAttentionNodeDao.getById(nodeId)
        assertNotNull(queriedNode)
        assertEquals(nodeId, queriedNode.id)
        assertEquals(node.sectionIdentifier, queriedNode.sectionIdentifier)
    }

    @Test
    fun createNode_insertToDb_tryToQuery() {
        val sectionIdentifier = "RandomSectionIdentifier"
        val node = UserAttentionNode(sectionIdentifier = sectionIdentifier)

        userAttentionNodeDao.insert(node)

        val queriedNode = userAttentionNodeDao.findBySectionIdentifier(sectionIdentifier)

        assertNotNull(queriedNode)
        assertEquals(node.sectionIdentifier, queriedNode!!.sectionIdentifier)
    }

    @Test
    fun createNode_insertToDb_addChild_insertToDb_queryParentById_shouldHaveOneChild() {
        val parentNode = UserAttentionNode(sectionIdentifier = "parentNode")
        val parentId = userAttentionNodeDao.insert(parentNode)

        val childNode = UserAttentionNode(sectionIdentifier = "childNode", parentNodeId = parentId)
        val childId = userAttentionNodeDao.insert(childNode)

        val nodeWithChildren = userAttentionNodeDao.getNodeWithChildrenAndEventsById(parentId)

        assertNotNull(nodeWithChildren)
        assertNotNull(nodeWithChildren?.parent)
        assertNotNull(nodeWithChildren?.children)
        assertTrue(nodeWithChildren!!.children.isNotEmpty())

        assertEquals(childId, nodeWithChildren.children[0].id)
        assertEquals(parentId, nodeWithChildren.children[0].parentNodeId)
    }

    @Test
    fun createNode_insertToDb_addMultipleChildren_insertToDb_queryParentById_shouldHaveMultipleChildren() {
        val parentNode = UserAttentionNode(sectionIdentifier = "parentNode")
        val parentId = userAttentionNodeDao.insert(parentNode)

        for (i in 0 until 10) {
            val childNode = UserAttentionNode(sectionIdentifier = "childNode_$i", parentNodeId = parentId)
            userAttentionNodeDao.insert(childNode)
        }

        val nodeWithChildren = userAttentionNodeDao.getNodeWithChildrenAndEventsById(parentId)

        assertNotNull(nodeWithChildren)
        assertNotNull(nodeWithChildren?.parent)
        assertNotNull(nodeWithChildren?.children)
        assertTrue(nodeWithChildren!!.children.isNotEmpty())
        assertEquals(10, nodeWithChildren.children.size)

        nodeWithChildren.children.forEach { childNode -> assertEquals(parentId, childNode.parentNodeId) }
    }

    @Test
    fun createNode_insertToDb_addChild_insertToDb_queryParentBySectionId_shouldHaveOneChild() {
        val parentSectionId = "parentNode"
        val parentNode = UserAttentionNode(sectionIdentifier = parentSectionId)
        val parentId = userAttentionNodeDao.insert(parentNode)

        val childNode = UserAttentionNode(sectionIdentifier = "childNode", parentNodeId = parentId)
        val childId = userAttentionNodeDao.insert(childNode)

        val nodeWithChildren = userAttentionNodeDao.getNodeWithChildrenAndEventsBySectionId(parentSectionId)

        assertNotNull(nodeWithChildren)
        assertNotNull(nodeWithChildren?.parent)
        assertNotNull(nodeWithChildren?.children)
        assertTrue(nodeWithChildren!!.children.isNotEmpty())

        assertEquals(childId, nodeWithChildren.children[0].id)
        assertEquals(parentId, nodeWithChildren.children[0].parentNodeId)
    }

    @Test
    fun createNode_insertToDb_addMultipleChildren_insertToDb_queryParentBySectionId_shouldHaveMultipleChildren() {
        val parentSectionId = "parentNode"
        val parentNode = UserAttentionNode(sectionIdentifier = parentSectionId)
        val parentId = userAttentionNodeDao.insert(parentNode)

        for (i in 0 until 10) {
            val childNode = UserAttentionNode(sectionIdentifier = "childNode_$i", parentNodeId = parentId)
            userAttentionNodeDao.insert(childNode)
        }

        val nodeWithChildren = userAttentionNodeDao.getNodeWithChildrenAndEventsBySectionId(parentSectionId)

        assertNotNull(nodeWithChildren)
        assertNotNull(nodeWithChildren?.parent)
        assertNotNull(nodeWithChildren?.children)
        assertTrue(nodeWithChildren!!.children.isNotEmpty())
        assertEquals(10, nodeWithChildren.children.size)

        nodeWithChildren.children.forEach { childNode -> assertEquals(parentId, childNode.parentNodeId) }
    }

}