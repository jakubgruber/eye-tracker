package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.eyetracker.android.persistence.EyeTrackerDatabase
import com.eyetracker.android.persistence.model.UserAttentionEvent
import com.eyetracker.android.persistence.model.UserAttentionNode
import junit.framework.Assert.*
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class UserAttentionNodeWithChildrenAndEventsTest {

    private lateinit var eyeTrackerDatabase: EyeTrackerDatabase
    private lateinit var userAttentionNodeDao: UserAttentionNodeDao
    private lateinit var userAttentionEventDao: UserAttentionEventDao

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getContext()
        eyeTrackerDatabase = Room.inMemoryDatabaseBuilder(context, EyeTrackerDatabase::class.java).build()
        userAttentionNodeDao = eyeTrackerDatabase.UserAttentionNodeDao()
        userAttentionEventDao = eyeTrackerDatabase.UserAttentionEventDao()
    }

    @After
    fun closeDb() {
        eyeTrackerDatabase.close()
    }

    @Test
    fun createNode_linkEvents_linkChildren_linkEvents_queryParent_shouldHaveChildren_shouldHaveEvents() {
        // create parentNode
        val parentNode = UserAttentionNode(sectionIdentifier = "SectionIdentifier")
        val parentNodeId = userAttentionNodeDao.insert(parentNode)

        // insert events
        for (i in 0 until 10) {
            val event = UserAttentionEvent(event = "OnAttentionLost", createdDate = Date(), userAttentionNodeId = parentNodeId)
            userAttentionEventDao.insert(event)
        }

        // insert children
        for (i in 0 until 10) {
            val childNode = UserAttentionNode(sectionIdentifier = "childNode_$i", parentNodeId = parentNodeId)
            userAttentionNodeDao.insert(childNode)
        }

        // query all
        val nodeWithChildrenAndEvents = userAttentionNodeDao.getNodeWithChildrenAndEventsBySectionId(parentNode.sectionIdentifier)

        // verify general
        assertNotNull(nodeWithChildrenAndEvents)
        assertNotNull(nodeWithChildrenAndEvents?.parent)

        // verify children
        assertNotNull(nodeWithChildrenAndEvents?.children)
        assertTrue(nodeWithChildrenAndEvents!!.children.isNotEmpty())
        assertEquals(10, nodeWithChildrenAndEvents.children.size)

        nodeWithChildrenAndEvents.children.forEach { childNode -> Assert.assertEquals(parentNodeId, childNode.parentNodeId) }

        // verify events
        assertNotNull(nodeWithChildrenAndEvents)
        assertNotNull(nodeWithChildrenAndEvents.events)
        assertEquals(10, nodeWithChildrenAndEvents.events.size)
    }

}