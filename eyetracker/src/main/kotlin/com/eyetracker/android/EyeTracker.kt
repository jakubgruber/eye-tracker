package com.eyetracker.android

import android.arch.persistence.room.Room
import android.content.Context
import com.eyetracker.android.communication.tree.UserAttentionTree
import com.eyetracker.android.persistence.EyeTrackerDatabase
import com.eyetracker.android.persistence.dao.UserAttentionNodeDao
import com.eyetracker.android.persistence.model.UserAttentionNode
import java.util.*

/**
 * Created by jakub on 03.04.2018.
 */
object EyeTracker {

    const val DATABASE_NAME = "eye_tracker_database"

    lateinit var database: EyeTrackerDatabase
    private val userAttentionNodeDao: UserAttentionNodeDao by lazy { database.UserAttentionNodeDao() }

    @JvmStatic
    fun init(context: Context) {
        database = Room.databaseBuilder(context, EyeTrackerDatabase::class.java, DATABASE_NAME)
                .allowMainThreadQueries().build()
    }

    @JvmStatic
    fun buildTree(sectionId: String): UserAttentionTree {
        val rootNode = userAttentionNodeDao.findBySectionIdentifier(sectionId)
                ?: return UserAttentionTree()

        val queue = PriorityQueue<UserAttentionNode>()
        queue.add(rootNode)

        while (queue.isNotEmpty()) {
            val node = queue.remove()

            val nodeParent = userAttentionNodeDao.getById(node.parentNodeId)
            node.parent = nodeParent

            val nodeChildrenAndEvents = userAttentionNodeDao.getNodeWithChildrenAndEventsById(node.id)
            nodeChildrenAndEvents?.events?.let { node.events = it }
            node.events.forEach { it.node = node }

            nodeChildrenAndEvents?.children?.let { node.children = it }
            node.children.forEach { it.parent = node }

            queue.addAll(node.children)
        }

        return UserAttentionTree(rootNode)
    }

}