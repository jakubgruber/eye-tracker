package com.eyetracker.android

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.eyetracker.android.camera.CameraController
import com.eyetracker.android.camera.graphic.CameraSourcePreview
import com.eyetracker.android.camera.graphic.GraphicOverlay
import com.eyetracker.android.communication.picture.PictureTakenListener
import com.eyetracker.android.communication.verification.UserVerificationListener
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.eyetracker.android.extension.inflate
import com.jakub.eyetracker.R
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.error
import org.jetbrains.anko.warn

class EyeTrackerFragment : Fragment(), AnkoLogger {

    private var cameraController: CameraController? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.fragment_face_tracker)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val cameraSourcePreview = view.findViewById<View>(R.id.preview) as CameraSourcePreview
        val graphicOverlay = view.findViewById<View>(R.id.faceOverlay) as GraphicOverlay

        if (cameraController == null) {
            cameraController = CameraController(Configuration.PREVIEW_ENABLED, cameraSourcePreview, graphicOverlay, context = context!!)
        }

        if (ActivityCompat.checkSelfPermission(context!!.applicationContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            cameraController?.createCameraSource(context!!)
        } else {
            requestCameraPermission()
        }
    }

    /**
     * Restarts the camera.
     */
    override fun onResume() {
        super.onResume()
        cameraController?.start(context!!, activity!!)
    }

    /**
     * Stops the camera.
     */
    override fun onPause() {
        super.onPause()
        cameraController?.stop()
    }

    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    override fun onDestroy() {
        super.onDestroy()
        cameraController?.stop()
    }

    fun registerUser(username: String, pictureListener: PictureTakenListener) {
        cameraController?.registerUser(username, pictureListener)
    }

    fun verifyUser(username: String, verificationListener: UserVerificationListener) {
        cameraController?.verifyUser(username, verificationListener)
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Toast" message of why the permission is needed then
     * sending the request.
     */
    private fun requestCameraPermission() {
        warn("Camera permission is not granted. Requesting permission")

        val permissions = arrayOf(Manifest.permission.CAMERA)

        if (!ActivityCompat.shouldShowRequestPermissionRationale(activity!!,
                        Manifest.permission.CAMERA)) {

            requestPermissions(permissions, Configuration.RC_HANDLE_CAMERA_PERM)
            return
        }

        Toast.makeText(context!!.applicationContext, "Give me permissions", Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != Configuration.RC_HANDLE_CAMERA_PERM) {
            debug("Got unexpected permission result: $requestCode")
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            debug("Camera permission granted - initialize the camera source")
            cameraController?.createCameraSource(context!!)
            cameraController?.start(context!!, activity!!)
            return
        }

        error("Permission not granted: results len = ${grantResults.size}. Result code = ${if (grantResults.isNotEmpty()) grantResults[0] else "(empty)"}")

        AlertDialog.Builder(context)
                .setTitle("Permissions not granted")
                .setMessage("Application cannot work without permissions")
                .setPositiveButton("Ok", { _, _ -> activity!!.finish() })
                .show()
    }

    object Builder {

        private var framesPerSecond: Float = Configuration.FRAMES_PER_SECOND
        private var previewEnabled: Boolean = Configuration.PREVIEW_ENABLED
        private var precisionLevel: EyeTrackingLevel = Configuration.PRECISION_LEVEL

        private var awsAccessKeyId = Security.AWS_ACCESS_KEY_ID
        private var awsSecretAccessKey = Security.AWS_SECRET_ACCESS_KEY

        fun withFps(fps: Float): Builder {
            framesPerSecond = fps
            return this
        }

        fun withPreview(enabled: Boolean): Builder {
            previewEnabled = enabled
            return this
        }

        fun withPrecision(level: EyeTrackingLevel): Builder {
            precisionLevel = level
            return this
        }

        fun withAwsAccessKeys(keyId: String, secretKey: String): Builder {
            awsAccessKeyId = keyId
            awsSecretAccessKey = secretKey
            return this
        }

        fun build(): EyeTrackerFragment {
            EyeTrackerFragment.Configuration.FRAMES_PER_SECOND = framesPerSecond
            EyeTrackerFragment.Configuration.PREVIEW_ENABLED = previewEnabled
            EyeTrackerFragment.Configuration.PRECISION_LEVEL = precisionLevel

            EyeTrackerFragment.Security.AWS_ACCESS_KEY_ID = awsAccessKeyId
            EyeTrackerFragment.Security.AWS_SECRET_ACCESS_KEY = awsSecretAccessKey

            return EyeTrackerFragment()
        }

    }

    object Configuration {
        var FRAMES_PER_SECOND = 30.0f
        var PREVIEW_ENABLED = false
        var PRECISION_LEVEL = EyeTrackingLevel.HIGH_PRECISION

        // permission request codes need to be < 256
        const val RC_HANDLE_CAMERA_PERM = 2
    }

    object Security {
        var AWS_ACCESS_KEY_ID = ""
        var AWS_SECRET_ACCESS_KEY = ""
    }

    companion object {
        val TAG = "com.eyetracker.android.EyeTrackerFragment"
    }

}
