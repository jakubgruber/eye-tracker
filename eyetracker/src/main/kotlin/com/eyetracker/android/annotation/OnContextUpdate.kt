package com.eyetracker.android.annotation

/**
 * Created by jakub on 14.02.2018.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class OnContextUpdate