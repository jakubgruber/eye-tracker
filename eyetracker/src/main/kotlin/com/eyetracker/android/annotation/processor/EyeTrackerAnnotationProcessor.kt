package com.eyetracker.android.annotation.processor

import com.eyetracker.android.EyeTracker
import com.eyetracker.android.annotation.OnContextUpdate
import com.eyetracker.android.annotation.OnFaceAppeared
import com.eyetracker.android.annotation.OnFaceLost
import com.eyetracker.android.annotation.OnYawnDetected
import com.eyetracker.android.communication.event.UserAttentionAwareComponent
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.persistence.model.UserAttentionEvent
import com.eyetracker.android.persistence.model.UserAttentionNode
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug
import org.jetbrains.anko.verbose
import org.jetbrains.anko.warn
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberFunctions
import kotlin.reflect.full.findAnnotation

/**
 * Created by jakub on 14.02.2018.
 */

object EyeTrackerAnnotationProcessor : AnkoLogger {

    private val userAttentionEventDao by lazy { EyeTracker.database.UserAttentionEventDao() }
    private val userAttentionNodeDao by lazy { EyeTracker.database.UserAttentionNodeDao() }

    private val subscriberSet = mutableSetOf<Any>()
    private val nodeMap = hashMapOf<String, UserAttentionNode>()

    /**
     * Adds instance to classes that will be notified about new context changes.
     *
     * If instance implements UserAttentionAwareComponent, new UserAttentionNode is created in database
     * and will be used to associate events
     */
    fun inject(instance: Any) {
        subscriberSet.add(instance)
        if (instance is UserAttentionAwareComponent) {
            val componentSectionId = instance.awareComponent().sectionIdentifier
            var existingNode = userAttentionNodeDao.findBySectionIdentifier(componentSectionId)
            if (existingNode == null) {
                debug("No record in database for node with sectionId - $componentSectionId. New will be created.")

                val parentSectionId = instance.awareComponent().parentSectionIdentifier
                if (parentSectionId != null) {
                    val parent = userAttentionNodeDao.findBySectionIdentifier(parentSectionId)
                    parent?.let {
                        debug("Parent with sectionId - {$parentSectionId} defined, linking new node with sectionId {$componentSectionId} with parent.")

                        val existingNodeId = userAttentionNodeDao.insert(UserAttentionNode(sectionIdentifier = componentSectionId, parentNodeId = parent.id))
                        existingNode = userAttentionNodeDao.getById(existingNodeId)
                    }
                    warn("Parent node was not found with sectionId - $parentSectionId")
                } else {
                    debug("Parent is not defined, creating simple node without parent.")
                    val existingNodeId = userAttentionNodeDao.insert(UserAttentionNode(sectionIdentifier = componentSectionId))
                    existingNode = userAttentionNodeDao.getById(existingNodeId)
                }
            }
            nodeMap[componentSectionId] = existingNode!!
        }
    }

    /**
     * Removes instance from set of classes notified by context updates.
     *
     * If it implements  UserAttentionAwareComponent, no events will be logged into database.
     */
    fun disconnect(instance: Any) {
        subscriberSet.remove(instance)
        if (instance is UserAttentionAwareComponent) {
            nodeMap.remove(instance.awareComponent().sectionIdentifier)
        }
    }

    /**
     * Notifies all classes that subscribed to context updates.
     *
     * If class implements UserAttentionAwareComponent, database event is created and associated
     * with given UserAttentionNode.
     */
    fun process(annotation: KClass<out Annotation>, eyeTrackerContext: EyeTrackerContext) {
        subscriberSet.forEach { instance ->
            run {
                instance::class.declaredMemberFunctions.forEach { f ->
                    run {
                        try {
                            when (annotation) {
                                OnFaceAppeared::class -> {
                                    f.findAnnotation<OnFaceAppeared>()?.let {
                                        logAttentionMovement(OnFaceAppeared::class.simpleName!!, instance)
                                        f.call(instance, eyeTrackerContext)
                                    }
                                }
                                OnFaceLost::class -> {
                                    f.findAnnotation<OnFaceLost>()?.let {
                                        logAttentionMovement(OnFaceLost::class.simpleName!!, instance)
                                        f.call(instance, eyeTrackerContext)
                                    }
                                }
                                OnContextUpdate::class -> {
                                    f.findAnnotation<OnContextUpdate>()?.let {
                                        f.call(instance, eyeTrackerContext)
                                    }
                                }
                                OnYawnDetected::class -> {
                                    f.findAnnotation<OnYawnDetected>()?.let {
                                        logAttentionMovement(OnYawnDetected::class.simpleName!!, instance)
                                        f.call(instance, eyeTrackerContext)
                                    }
                                }
                                else -> {
                                    verbose("Method does not specify any allowed annotation.")
                                }
                            }
                        } catch (e: Exception) {
                            warn("Exception occurred when notifying listeners.", e)
                        }

                    }
                }
            }
        }
    }

    private fun logAttentionMovement(event: String, instanceToLog: Any) {
        async(CommonPool) {
            if (instanceToLog is UserAttentionAwareComponent) {
                nodeMap[instanceToLog.awareComponent().sectionIdentifier]?.let {
                    userAttentionEventDao.insert(UserAttentionEvent(event = event, userAttentionNodeId = it.id))
                }
            }
        }
    }

}