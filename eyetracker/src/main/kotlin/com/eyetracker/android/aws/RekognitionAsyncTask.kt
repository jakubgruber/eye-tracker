package com.eyetracker.android.aws

import android.os.AsyncTask
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.rekognition.AmazonRekognitionClient
import com.amazonaws.services.rekognition.model.CompareFacesRequest
import com.amazonaws.services.rekognition.model.Image
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.communication.verification.UserVerificationResult
import java.nio.ByteBuffer

/**
 * Created by jakub on 28.12.2017.
 */
class RekognitionAsyncTask(private val requestData: RekognitionRequestData) : AsyncTask<Void, Void, Void>() {

    private val similarityThreshold = 80.toFloat()

    override fun doInBackground(vararg params: Void?): Void? {
        val accessKeyId = EyeTrackerFragment.Security.AWS_ACCESS_KEY_ID
        val secretAccessKey = EyeTrackerFragment.Security.AWS_SECRET_ACCESS_KEY

        val awsCredentials = BasicAWSCredentials(accessKeyId, secretAccessKey)
        val rekognitionClient = AmazonRekognitionClient(awsCredentials)

        val wrappedBytes = ByteBuffer.wrap(requestData.imageBytes)
        val wrappedToCompareBytes = ByteBuffer.wrap(requestData.imageToCompareBytes)

        val source = Image().withBytes(wrappedBytes)
        val target = Image().withBytes(wrappedToCompareBytes)

        val request = CompareFacesRequest()
                .withSourceImage(source)
                .withTargetImage(target)
                .withSimilarityThreshold(similarityThreshold)

        try {
            val compareFacesResult = rekognitionClient.compareFaces(request)

            if (compareFacesResult.faceMatches.isNotEmpty()) {
                requestData.callback.onUserVerified(UserVerificationResult.VERIFIED)
            } else {
                requestData.callback.onUserVerified(UserVerificationResult.NOT_VERIFIED)
            }
        } catch (e: Exception) {
            requestData.callback.onUserVerified(UserVerificationResult.UNEXPECTED_ERROR)
        }

        return null
    }

}