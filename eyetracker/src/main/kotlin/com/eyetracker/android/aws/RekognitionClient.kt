package com.eyetracker.android.aws

import android.content.Context


/**
 * Created by jakub on 28.12.2017.
 */
object RekognitionClient {

    fun compare(requestData: RekognitionRequestData) {
        RekognitionAsyncTask(requestData).execute()
    }

}