package com.eyetracker.android.aws

import com.eyetracker.android.communication.verification.UserVerificationListener

/**
 * Created by jakub on 30.12.2017.
 */
data class RekognitionRequestData(val imageBytes: ByteArray, val imageToCompareBytes: ByteArray, val callback: UserVerificationListener)