package com.eyetracker.android.camera

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import com.eyetracker.android.camera.graphic.CameraSourcePreview
import com.eyetracker.android.camera.graphic.GraphicOverlay
import com.eyetracker.android.camera.processor.FaceDetectionsProcessor
import com.eyetracker.android.camera.tracker.AbstractFaceTracker
import com.eyetracker.android.camera.tracker.FaceTracker
import com.eyetracker.android.camera.tracker.GraphicFaceTracker
import com.eyetracker.android.communication.picture.PictureTakenListener
import com.eyetracker.android.communication.picture.TakePictureResult
import com.eyetracker.android.communication.verification.UserVerificationListener
import com.eyetracker.android.communication.verification.UserVerificationResult
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.face.FaceDetector
import com.google.android.gms.vision.face.LargestFaceFocusingProcessor
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.warn
import java.io.IOException
import java.io.Serializable


/**
 * Created by jakub on 01.12.2017.
 */
class CameraController(private val usePreview: Boolean,
                       private var cameraSourcePreview: CameraSourcePreview,
                       private var graphicOverlay: GraphicOverlay,
                       trackingLevel: EyeTrackingLevel = EyeTrackingLevel.HIGH_PRECISION,
                       context: Context) : AnkoLogger, Serializable {

    private val faceDetectionProcessor = FaceDetectionsProcessor(trackingLevel)
    private val photoController = PhotoController(context)

    private var cameraSource: CameraSource? = null

    init {
        if (!usePreview) {
            cameraSourcePreview.visibility = View.GONE
            graphicOverlay.visibility = View.GONE
        }
    }

    fun createCameraSource(context: Context) {
        val detector = FaceDetector.Builder(context.applicationContext)
                .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                .setProminentFaceOnly(true)
                .setTrackingEnabled(true)
                .setMode(FaceDetector.ACCURATE_MODE)
                .build()

        val mobileVisionFaceProcessor = LargestFaceFocusingProcessor(detector, initFaceTracker())
        detector.setProcessor(mobileVisionFaceProcessor)

        if (!detector.isOperational) {
            warn("Face detector dependencies are not yet available.")
        }

        cameraSource = CameraSource.Builder(context, detector)
                .setRequestedPreviewSize(640, 480)
                .setFacing(CameraSource.CAMERA_FACING_FRONT)
                .setRequestedFps(30.0f)
                .build()
    }

    @SuppressLint("MissingPermission")
    fun start(context: Context, activity: Activity) {
        val code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                context.applicationContext)
        if (code != ConnectionResult.SUCCESS) {
            val dlg = GoogleApiAvailability.getInstance().getErrorDialog(activity, code, RC_HANDLE_GMS)
            dlg.show()
        }

        cameraSource?.let {
            try {
                startCameraInternal()
            } catch (e: IOException) {
                error("Unable to start camera source.", e)
                release()
                cameraSource = null
            }

        }
    }

    fun stop() {
        if (usePreview) {
            cameraSourcePreview.stop()
        } else {
            cameraSource?.stop()
        }
    }

    fun release() {
        cameraSource?.release()
    }

    fun registerUser(username: String, pictureListener: PictureTakenListener) {
        val processedUsername = preprocessUsername(username)
        if (processedUsername.isEmpty()) {
            pictureListener.onPictureTaken(null, TakePictureResult.NULL_USERNAME)
            return
        }
        try {
            cameraSource?.takePicture(null, { imageByteArray ->
                run {
                    if (imageByteArray != null) {
                        val rotatedBitmap = photoController.persist(imageByteArray, processedUsername)
                        pictureListener.onPictureTaken(rotatedBitmap, TakePictureResult.OK)
                    }
                }
            })
        } catch (e: Exception) {
            error("Could not take picture")
            pictureListener.onPictureTaken(null, TakePictureResult.UNEXPECTED_ERROR)
        }
    }

    fun verifyUser(username: String, verificationListener: UserVerificationListener) {
        val processedUsername = preprocessUsername(username)
        try {
            cameraSource?.takePicture(null, { imageByteArray ->
                run {
                    if (imageByteArray != null) {
                        photoController.compare(imageByteArray, processedUsername, verificationListener)
                    }
                }
            })
        } catch (e: Exception) {
            error("Could not verify user - $e")
            verificationListener.onUserVerified(UserVerificationResult.UNEXPECTED_ERROR)
        }
    }

    @SuppressLint("MissingPermission")
    private fun startCameraInternal() {
        if (usePreview) {
            cameraSourcePreview.start(cameraSource!!, graphicOverlay)
        } else {
            cameraSource?.start()
        }
    }

    private fun initFaceTracker(): AbstractFaceTracker {
        return if (usePreview) {
            GraphicFaceTracker(faceDetectionProcessor, graphicOverlay)
        } else {
            FaceTracker(faceDetectionProcessor)
        }
    }

    private fun preprocessUsername(username: String): String {
        val regex = Regex("[^A-Za-z]")
        return regex.replace(username, "")
    }

    companion object {
        // google play services error code
        private val RC_HANDLE_GMS = 9001
    }

}