package com.eyetracker.android.camera

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ExifInterface
import com.eyetracker.android.EyeTracker
import com.eyetracker.android.aws.RekognitionClient
import com.eyetracker.android.aws.RekognitionRequestData
import com.eyetracker.android.communication.verification.UserVerificationListener
import com.eyetracker.android.communication.verification.UserVerificationResult
import com.eyetracker.android.extension.compressToArray
import com.eyetracker.android.extension.rotate
import com.eyetracker.android.persistence.model.Photo
import java.io.File
import java.util.*


/**
 * Created by jakub on 18.12.2017.
 */
class PhotoController(private val context: Context) {

    private val photoDao by lazy { EyeTracker.database.PhotoDao() }

    fun persist(photoBytes: ByteArray, username: String): Bitmap {
        // check if photo exists
        val photoExists = photoDao.findByUsername(username) != null

        val file = prepareFile(username, photoExists)
        writeFile(photoBytes, file)
        val filePath = file.absolutePath

        val originalPhotoBitmap = BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.size)
        val rotatedBitmap = rotateIfNecessary(filePath, originalPhotoBitmap)

        val compressedPhotoBytes = rotatedBitmap.compressToArray()
        writeFile(compressedPhotoBytes, file)

        if (!photoExists) {
            photoDao.insert(Photo(username = username, path = filePath))
        }

        return rotatedBitmap
    }

    fun compare(photoBytes: ByteArray, username: String, verificationListener: UserVerificationListener) {
        val queriedPhoto = photoDao.findByUsername(username)
        queriedPhoto ?: run {
            verificationListener.onUserVerified(UserVerificationResult.NO_INFORMATION)
            return
        }

        // process original photo -> load, compress
        val oldPhotoBitmap = BitmapFactory.decodeFile(queriedPhoto.path)
        val oldPhotoBytes = oldPhotoBitmap.compressToArray()

        // process new photo -> save, rotate, compress, delete
        val tempFile = prepareFile("tmp_image_${UUID.randomUUID()}", false)
        writeFile(photoBytes, tempFile)

        val newPhotoBitmap = BitmapFactory.decodeByteArray(photoBytes, 0, photoBytes.size)
        val newRotatedBitmap = rotateIfNecessary(tempFile.absolutePath, newPhotoBitmap)
        val newCompressedPhotoBytes = newRotatedBitmap.compressToArray()

        tempFile.delete()

        val requestData = RekognitionRequestData(oldPhotoBytes, newCompressedPhotoBytes, verificationListener)
        RekognitionClient.compare(requestData)
    }

    private fun rotateIfNecessary(filePath: String, originalPhoto: Bitmap): Bitmap {
        val ei = ExifInterface(filePath)
        val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED)

        var rotatedBitmap = originalPhoto
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> {
                rotatedBitmap = originalPhoto.rotate(90.toFloat())
            }
            ExifInterface.ORIENTATION_ROTATE_180 -> {
                rotatedBitmap = originalPhoto.rotate(180.toFloat())
            }
            ExifInterface.ORIENTATION_ROTATE_270 -> {
                rotatedBitmap = originalPhoto.rotate(270.toFloat())
            }
        }

        return rotatedBitmap
    }

    private fun prepareFile(fileName: String, removeFile: Boolean): File {
        if (removeFile) {
            File(context.filesDir, fileName).delete()
        }
        return File(context.filesDir, fileName)
    }

    private fun writeFile(photoBytes: ByteArray, photoFile: File) {
        val os = photoFile.outputStream()
        os.write(photoBytes)
        os.close()
    }

}