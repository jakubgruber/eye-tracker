package com.eyetracker.android.camera.graphic

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.google.android.gms.vision.face.Face

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class FaceGraphic(overlay: GraphicOverlay) : GraphicOverlay.Graphic(overlay) {

    private val facePositionPaint: Paint
    private val idPaint: Paint
    private val boxPaint: Paint

    @Volatile
    private var detectedFace: Face? = null
    private var faceId: Int = 0

    init {
        val textColor = Color.BLUE

        facePositionPaint = Paint()
        facePositionPaint.color = textColor

        idPaint = Paint()
        idPaint.color = textColor
        idPaint.textSize = 40.0f

        boxPaint = Paint()
        boxPaint.color = textColor
        boxPaint.style = Paint.Style.STROKE
        boxPaint.strokeWidth = BOX_STROKE_WIDTH
    }

    fun setId(id: Int) {
        faceId = id
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    fun updateFace(face: Face?) {
        detectedFace = face
        postInvalidate()
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    override fun draw(canvas: Canvas) {
        val face = detectedFace ?: return

        // Draws a circle at the position of the detected face, with the face's track id below.
        val x = translateX(face.position.x + face.width / 2)
        val y = translateY(face.position.y + face.height / 2)
        canvas.drawCircle(x, y, FACE_POSITION_RADIUS, facePositionPaint)
        canvas.drawText("id: " + faceId, x + ID_X_OFFSET, y + ID_Y_OFFSET, idPaint)
        canvas.drawText("happiness: " + String.format("%.2f", face.isSmilingProbability), x - ID_X_OFFSET, y - ID_Y_OFFSET, idPaint)
        canvas.drawText("right eye: " + String.format("%.2f", face.isRightEyeOpenProbability), x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, idPaint)
        canvas.drawText("left eye: " + String.format("%.2f", face.isLeftEyeOpenProbability), x - ID_X_OFFSET * 2, y - ID_Y_OFFSET * 2, idPaint)

        // Draws a bounding box around the face.
        val xOffset = scaleX(face.width / 2.0f)
        val yOffset = scaleY(face.height / 2.0f)
        val left = x - xOffset
        val top = y - yOffset
        val right = x + xOffset
        val bottom = y + yOffset
        canvas.drawRect(left, top, right, bottom, boxPaint)
    }

    private companion object {
        private val FACE_POSITION_RADIUS = 10.0f
        private val ID_Y_OFFSET = 50.0f
        private val ID_X_OFFSET = -50.0f
        private val BOX_STROKE_WIDTH = 5.0f
    }
}
