package com.eyetracker.android.camera.processor

import com.eyetracker.android.annotation.OnContextUpdate
import com.eyetracker.android.annotation.OnFaceAppeared
import com.eyetracker.android.annotation.OnFaceLost
import com.eyetracker.android.annotation.OnYawnDetected
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.camera.processor.helper.FaceDetectionsProcessorHelper
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.google.android.gms.vision.face.Face
import org.jetbrains.anko.AnkoLogger

/**
 * Created by jakub on 02.12.2017.
 */
class FaceDetectionsProcessor(private val trackingLevel: EyeTrackingLevel) : AnkoLogger {

    private val eyeContext = EyeTrackerContext()
    private val faceDetectionsHelper = FaceDetectionsProcessorHelper()

    fun notifyNewItemDetected(item: Face) {
        if (trackingLevel != EyeTrackingLevel.DO_NOT_DISTURB) {
            faceDetectionsHelper.processNewFace(item, eyeContext)

            EyeTrackerAnnotationProcessor.process(OnFaceAppeared::class, eyeContext)
        }
    }

    fun notifyFaceUpdated(face: Face) {
        if (trackingLevel == EyeTrackingLevel.HIGH_PRECISION) {
            faceDetectionsHelper.processUpdate(face, eyeContext)

            if (eyeContext.yawnDetected) {
                EyeTrackerAnnotationProcessor.process(OnYawnDetected::class, eyeContext)
            }

            EyeTrackerAnnotationProcessor.process(OnContextUpdate::class, eyeContext)
        }
    }

    fun notifyFaceLost() {
        if (trackingLevel != EyeTrackingLevel.DO_NOT_DISTURB) {
            faceDetectionsHelper.processFaceLost(eyeContext)

            EyeTrackerAnnotationProcessor.process(OnFaceLost::class, eyeContext)
        }
    }

}