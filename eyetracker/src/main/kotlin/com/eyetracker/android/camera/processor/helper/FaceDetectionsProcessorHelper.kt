package com.eyetracker.android.camera.processor.helper

import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.extension.isPossibleToDetectYawn
import com.google.android.gms.vision.face.Face
import com.google.android.gms.vision.face.Landmark
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.debug

/**
 * Created by jakub on 04.12.2017.
 */
class FaceDetectionsProcessorHelper : AnkoLogger {

    private val OPEN_EYE_PROBABILITY_THRESHOLD = 0.6
    private val YAWN_DETECTION_THRESHOLD = 55

    private var eyesOpen = true

    fun processNewFace(face: Face, eyeContext: EyeTrackerContext) {
        processUpdate(face, eyeContext)
    }

    fun processUpdate(face: Face, eyeContext: EyeTrackerContext) {
        eyeContext.facePresent = true
        eyeContext.leftEyeOpen = (face.isLeftEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD)
        eyeContext.rightEyeOpen = (face.isLeftEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD)
        eyeContext.blinkDetected = detectBlink(face)
        eyeContext.yawnDetected = detectYawn(face)

        updateOpenEyesIndicator(face)
    }

    fun processFaceLost(eyeContext: EyeTrackerContext) {
        eyeContext.facePresent = false
        eyeContext.leftEyeOpen = false
        eyeContext.rightEyeOpen = false
    }

    private fun detectBlink(face: Face) = (
            !eyesOpen
                    && face.isLeftEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD
                    && face.isRightEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD)

    private fun updateOpenEyesIndicator(face: Face) {
        eyesOpen = face.isLeftEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD && face.isLeftEyeOpenProbability > OPEN_EYE_PROBABILITY_THRESHOLD
    }

    private fun detectYawn(face: Face): Boolean {
        val bottomMouth = face.landmarks.find { landmark -> landmark.type == Landmark.BOTTOM_MOUTH }
        val leftMouth = face.landmarks.find { landmark -> landmark.type == Landmark.LEFT_MOUTH }
        val rightMouth = face.landmarks.find { landmark -> landmark.type == Landmark.RIGHT_MOUTH }

        if (!face.isPossibleToDetectYawn()) {
            debug("Cannot detect if mouth is open, missing one of landmarks.")
            return false
        }

        val cBottomMouthY = bottomMouth!!.position.y.toInt()
        val cLeftMouthY = leftMouth!!.position.y.toInt()
        val cRightMouthY = rightMouth!!.position.y.toInt()
        val centerPointY = ((cLeftMouthY + cRightMouthY) / 2 - 20).toFloat()

        return if (Math.abs(centerPointY - cBottomMouthY) > YAWN_DETECTION_THRESHOLD) {
            debug("Mouth is opened -> yawn detected")
            true
        } else {
            debug("Mouth not opened")
            false
        }
    }

}