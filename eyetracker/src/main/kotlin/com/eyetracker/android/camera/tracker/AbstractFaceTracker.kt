package com.eyetracker.android.camera.tracker

import com.eyetracker.android.camera.processor.FaceDetectionsProcessor
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Tracker
import com.google.android.gms.vision.face.Face

/**
 * Created by jakub on 01.12.2017.
 */
abstract class AbstractFaceTracker(private val processor: FaceDetectionsProcessor) : Tracker<Face>() {

    override fun onNewItem(faceId: Int, item: Face) {
        processor.notifyNewItemDetected(item)
        newItemInternal(faceId, item)
    }

    override fun onUpdate(detectionResults: Detector.Detections<Face>, face: Face) {
        processor.notifyFaceUpdated(face)
        updateInternal(detectionResults, face)
    }

    override fun onMissing(detectionResults: Detector.Detections<Face>) {
        missingInternal(detectionResults)
    }

    override fun onDone() {
        processor.notifyFaceLost()
        doneInternal()
    }

    abstract fun newItemInternal(faceId: Int, item: Face)

    abstract fun updateInternal(detectionResults: Detector.Detections<Face>, face: Face)

    abstract fun missingInternal(detectionResults: Detector.Detections<Face>)

    abstract fun doneInternal()

}
