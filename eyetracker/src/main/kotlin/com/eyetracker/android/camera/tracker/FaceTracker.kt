package com.eyetracker.android.camera.tracker

import com.eyetracker.android.camera.processor.FaceDetectionsProcessor
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.face.Face

/**
 * Created by jakub on 01.12.2017.
 */
class FaceTracker(processor: FaceDetectionsProcessor) : AbstractFaceTracker(processor) {

    override fun newItemInternal(faceId: Int, item: Face) {}

    override fun updateInternal(detectionResults: Detector.Detections<Face>, face: Face) {}

    override fun missingInternal(detectionResults: Detector.Detections<Face>) {}

    override fun doneInternal() {}

}