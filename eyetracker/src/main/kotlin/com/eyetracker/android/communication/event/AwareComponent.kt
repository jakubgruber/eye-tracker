package com.eyetracker.android.communication.event

/**
 * Created by jakub on 28.03.2018.
 */
data class AwareComponent(
        val sectionIdentifier: String,
        val parentSectionIdentifier: String? = null)