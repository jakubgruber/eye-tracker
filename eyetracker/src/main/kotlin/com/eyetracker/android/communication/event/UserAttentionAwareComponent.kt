package com.eyetracker.android.communication.event

/**
 * Created by jakub on 22.03.2018.
 */
interface UserAttentionAwareComponent {

    fun awareComponent(): AwareComponent

}