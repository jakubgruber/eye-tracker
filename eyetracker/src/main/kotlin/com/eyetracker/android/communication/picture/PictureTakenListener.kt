package com.eyetracker.android.communication.picture

import android.graphics.Bitmap

/**
 * Created by jakub on 17.12.2017.
 */
interface PictureTakenListener {

    fun onPictureTaken(image: Bitmap?, result: TakePictureResult)

}