package com.eyetracker.android.communication.picture

/**
 * Created by jakub on 30.12.2017.
 */
enum class TakePictureResult {

    OK,
    NULL_USERNAME,
    UNEXPECTED_ERROR
}