package com.eyetracker.android.communication.tracker

/**
 * Created by jakub on 18.11.2017.
 */
class EyeTrackerContext {

    var facePresent = false
    var leftEyeOpen = false
    var rightEyeOpen = false
    var blinkDetected = false
    var yawnDetected = false

}