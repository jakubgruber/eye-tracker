package com.eyetracker.android.communication.tree

import com.eyetracker.android.persistence.model.UserAttentionNode

data class UserAttentionTree(var root: UserAttentionNode? = null)