package com.eyetracker.android.communication.verification

/**
 * Created by jakub on 28.12.2017.
 */
interface UserVerificationListener {

    fun onUserVerified(result: UserVerificationResult)

}