package com.eyetracker.android.communication.verification

/**
 * Created by jakub on 28.12.2017.
 */
enum class UserVerificationResult {

    VERIFIED,
    NOT_VERIFIED,
    NO_INFORMATION,
    UNEXPECTED_ERROR

}