package com.eyetracker.android.configuration

/**
 * Created by jakub on 16.12.2017.
 */
enum class EyeTrackingLevel {

    HIGH_PRECISION,
    MAJOR_EVENTS,
    DO_NOT_DISTURB

}