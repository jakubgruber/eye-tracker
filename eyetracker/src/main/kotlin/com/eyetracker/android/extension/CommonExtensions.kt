package com.eyetracker.android.extension

import android.graphics.Bitmap
import android.graphics.Matrix
import android.support.annotation.LayoutRes
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.vision.face.Face
import com.google.android.gms.vision.face.Landmark
import java.io.ByteArrayOutputStream

/**
 * Created by jakub on 02.12.2017.
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View =
        LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
    val fragmentTransaction = beginTransaction()
    fragmentTransaction.func()
    fragmentTransaction.commit()
}

fun Bitmap.compressToArray(): ByteArray {
    val compressPhotoStream = ByteArrayOutputStream()
    this.compress(Bitmap.CompressFormat.JPEG, 50, compressPhotoStream)

    return compressPhotoStream.toByteArray()
}

fun Bitmap.rotate(angle: Float): Bitmap {
    val matrix = Matrix()
    matrix.setRotate(angle)
    return Bitmap.createBitmap(this, 0, 0, this.width, this.height,
            matrix, true)
}

fun Face.isPossibleToDetectYawn(): Boolean {
    val bottomMouth = this.landmarks.find { landmark -> landmark.type == Landmark.BOTTOM_MOUTH }
    val leftMouth = this.landmarks.find { landmark -> landmark.type == Landmark.LEFT_MOUTH }
    val rightMouth = this.landmarks.find { landmark -> landmark.type == Landmark.RIGHT_MOUTH }

    return !(bottomMouth == null || leftMouth == null || rightMouth == null)
}