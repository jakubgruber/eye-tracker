package com.eyetracker.android.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.eyetracker.android.persistence.converter.DateToLongConverter
import com.eyetracker.android.persistence.dao.PhotoDao
import com.eyetracker.android.persistence.dao.UserAttentionEventDao
import com.eyetracker.android.persistence.dao.UserAttentionNodeDao
import com.eyetracker.android.persistence.model.Photo
import com.eyetracker.android.persistence.model.UserAttentionEvent
import com.eyetracker.android.persistence.model.UserAttentionNode

/**
 * Created by jakub on 03.04.2018.
 */
@Database(
        entities = arrayOf(Photo::class, UserAttentionEvent::class, UserAttentionNode::class),
        version = 1,
        exportSchema = false
)
@TypeConverters(DateToLongConverter::class)
abstract class EyeTrackerDatabase : RoomDatabase() {

    abstract fun PhotoDao(): PhotoDao

    abstract fun UserAttentionEventDao(): UserAttentionEventDao

    abstract fun UserAttentionNodeDao(): UserAttentionNodeDao
}
