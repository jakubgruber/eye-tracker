package com.eyetracker.android.persistence.converter

import android.arch.persistence.room.TypeConverter
import java.util.*


/**
 * Created by jakub on 03.04.2018.
 */
class DateToLongConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

}