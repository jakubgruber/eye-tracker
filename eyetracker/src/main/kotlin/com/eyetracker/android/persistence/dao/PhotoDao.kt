package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.eyetracker.android.persistence.model.Photo

/**
 * Created by jakub on 03.04.2018.
 */
@Dao
abstract class PhotoDao : BaseDao<Photo> {

    @Query("SELECT * FROM ${Photo.TABLE_NAME} WHERE ${Photo.COLUMN_USERNAME} = :username")
    abstract fun findByUsername(username: String): Photo?

}