package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Dao
import com.eyetracker.android.persistence.model.UserAttentionEvent

/**
 * Created by jakub on 03.04.2018.
 */
@Dao
abstract class UserAttentionEventDao : BaseDao<UserAttentionEvent>