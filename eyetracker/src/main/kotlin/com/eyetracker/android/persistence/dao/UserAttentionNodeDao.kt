package com.eyetracker.android.persistence.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import android.arch.persistence.room.Transaction
import com.eyetracker.android.persistence.model.UserAttentionNode
import com.eyetracker.android.persistence.model.UserAttentionNodeWithChildrenAndEvents

/**
 * Created by jakub on 03.04.2018.
 */
@Dao
abstract class UserAttentionNodeDao : BaseDao<UserAttentionNode> {

    @Query("SELECT * FROM ${UserAttentionNode.TABLE_NAME} WHERE ${UserAttentionNode.COLUMN_ID} = :nodeId")
    abstract fun getById(nodeId: Long): UserAttentionNode

    @Query("SELECT * FROM ${UserAttentionNode.TABLE_NAME} " +
            "WHERE ${UserAttentionNode.COLUMN_SECTION_IDENTIFIER} = :sectionIdentifier")
    abstract fun findBySectionIdentifier(sectionIdentifier: String): UserAttentionNode?

    @Transaction
    @Query("SELECT * FROM ${UserAttentionNode.TABLE_NAME} WHERE ${UserAttentionNode.COLUMN_ID} = :nodeId")
    abstract fun getNodeWithChildrenAndEventsById(nodeId: Long): UserAttentionNodeWithChildrenAndEvents?

    @Transaction
    @Query("SELECT * FROM ${UserAttentionNode.TABLE_NAME} WHERE ${UserAttentionNode.COLUMN_ID} = (SELECT ${UserAttentionNode.COLUMN_ID} FROM ${UserAttentionNode.TABLE_NAME} WHERE ${UserAttentionNode.COLUMN_SECTION_IDENTIFIER} = :sectionId)")
    abstract fun getNodeWithChildrenAndEventsBySectionId(sectionId: String): UserAttentionNodeWithChildrenAndEvents?

}