package com.eyetracker.android.persistence.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.eyetracker.android.persistence.model.Photo.Companion.COLUMN_PATH
import com.eyetracker.android.persistence.model.Photo.Companion.COLUMN_USERNAME

/**
 * Created by jakub on 03.04.2018.
 */
@Entity(tableName = Photo.TABLE_NAME,
        indices = arrayOf(
                Index(value = COLUMN_USERNAME, unique = true),
                Index(value = COLUMN_PATH, unique = true)))
data class Photo(

        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        @ColumnInfo(name = COLUMN_USERNAME)
        var username: String = "",
        @ColumnInfo(name = COLUMN_PATH)
        var path: String = "") {

    companion object {
        const val TABLE_NAME = "photo"
        const val COLUMN_USERNAME = "username"
        const val COLUMN_PATH = "path"
    }

}