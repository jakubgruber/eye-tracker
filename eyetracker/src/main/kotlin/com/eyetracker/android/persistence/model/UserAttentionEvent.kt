package com.eyetracker.android.persistence.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.eyetracker.android.persistence.model.UserAttentionEvent.Companion.TABLE_NAME
import java.util.*

/**
 * Created by jakub on 03.04.2018.
 */
@Entity(tableName = TABLE_NAME)
data class UserAttentionEvent(

        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        var event: String = "",
        var createdDate: Date = Date(),
        @ColumnInfo(name = COLUMN_USER_ATTENTION_NODE_ID)
        var userAttentionNodeId: Long = 0

) {
    companion object {
        const val TABLE_NAME = "user_attention_event"
        const val COLUMN_USER_ATTENTION_NODE_ID = "user_attention_node_id"
    }

    @Ignore
    lateinit var node: UserAttentionNode

}