package com.eyetracker.android.persistence.model

import android.arch.persistence.room.*
import com.eyetracker.android.persistence.model.UserAttentionNode.Companion.COLUMN_SECTION_IDENTIFIER

/**
 * Created by jakub on 03.04.2018.
 */
@Entity(tableName = UserAttentionNode.TABLE_NAME,
        indices = arrayOf(Index(value = arrayOf(COLUMN_SECTION_IDENTIFIER), unique = true)))
data class UserAttentionNode(

        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = COLUMN_ID)
        var id: Long = 0,
        @ColumnInfo(name = COLUMN_SECTION_IDENTIFIER)
        var sectionIdentifier: String = "",
        @ColumnInfo(name = COLUMN_PARENT_ID)
        var parentNodeId: Long = 0
) {
    companion object {
        const val TABLE_NAME = "user_attention_node"
        const val COLUMN_ID = "id"
        const val COLUMN_SECTION_IDENTIFIER = "section_identifier"
        const val COLUMN_PARENT_ID = "parent_node_id"
    }

    @Ignore
    var parent: UserAttentionNode? = null

    @Ignore
    var children: List<UserAttentionNode> = emptyList()

    @Ignore
    var events: List<UserAttentionEvent> = emptyList()

}