package com.eyetracker.android.persistence.model

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation

class UserAttentionNodeWithChildrenAndEvents {

    @Embedded
    var parent: UserAttentionNode? = null

    @Relation(parentColumn = UserAttentionNode.COLUMN_ID,
            entityColumn = UserAttentionNode.COLUMN_PARENT_ID)
    var children: List<UserAttentionNode> = emptyList()

    @Relation(entity = UserAttentionEvent::class,
            parentColumn = UserAttentionNode.COLUMN_ID,
            entityColumn = UserAttentionEvent.COLUMN_USER_ATTENTION_NODE_ID)
    var events: List<UserAttentionEvent> = emptyList()

}