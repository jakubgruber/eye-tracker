package com.eyetracker.android.demo.authentication.dynamic

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import me.jakub.dynamicauthentication.R

class AuthenticationFragment : Fragment() {

    private lateinit var authenticateButton: Button
    private lateinit var infoTextView: TextView

    private var callback: OnAuthenticationInitiatedListener? = null

    private var message: String = ""

    interface OnAuthenticationInitiatedListener {
        fun onAuthenticationInitiated()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_authentication, container, false)
        authenticateButton = view.findViewById(R.id.button_authenticate)
        authenticateButton.setOnClickListener({
            callback?.onAuthenticationInitiated()
        })

        infoTextView = view.findViewById(R.id.textview_info)
        infoTextView.text = message

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAuthenticationInitiatedListener) {
            callback = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnAuthenticationInitiatedListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    companion object {
        val TAG = "AuthenticationFragment"

        fun newInstance(message: String): AuthenticationFragment {
            val fragment = AuthenticationFragment()
            fragment.message = message
            return fragment
        }

    }

}
