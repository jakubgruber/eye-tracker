package com.eyetracker.android.demo.authentication.dynamic

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.eyetracker.android.EyeTracker
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.communication.picture.PictureTakenListener
import com.eyetracker.android.communication.picture.TakePictureResult
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.eyetracker.android.extension.inTransaction
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import me.jakub.dynamicauthentication.R
import me.jakub.dynamicauthentication.domain.DynamicUser
import me.jakub.dynamicauthentication.helper.PropertyReader
import org.jetbrains.anko.toast
import java.util.*


class FirebaseLoginActivity : AppCompatActivity(), AuthenticationFragment.OnAuthenticationInitiatedListener, PictureTakenListener {

    private val SIGN_IN_RC = 123

    private var loggedInUser: DynamicUser? = null

    private var eyeTrackerFragment: EyeTrackerFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_login)

        initiateEyeTracking()
    }

    override fun onStart() {
        super.onStart()

        EyeTracker.init(this)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser != null) {
            goToSecuredSection(currentUser)
            startDynamicAuthenticationControl(currentUser.email!!)
        }

        // Create and launch sign-in fragment
        FirebaseAuth.getInstance().currentUser ?: let {
            showAuthenticationWithMessage("Please sign in!")
        }
    }

    override fun onResume() {
        super.onResume()

        EyeTrackerAnnotationProcessor.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        EyeTrackerAnnotationProcessor.disconnect(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.logout_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        when (item.getItemId()) {
            R.id.logout -> {
                logout()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SIGN_IN_RC) {
            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                val user = FirebaseAuth.getInstance().currentUser

                goToSecuredSection(user!!)
            } else {
                showAuthenticationWithMessage("Sign in failed, please try again!")
            }
        }
    }

    override fun onAuthenticationInitiated() {
        detachAuthenticationFragment()
        // initiate sign-in via Firebase
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(Arrays.asList(
                                AuthUI.IdpConfig.FacebookBuilder().build()))
                        .build(),
                SIGN_IN_RC)
    }

    private fun logout() {
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    val signedInFragment = supportFragmentManager.findFragmentByTag(SignedInFragment.TAG)
                    signedInFragment?.let {
                        supportFragmentManager.beginTransaction().remove(signedInFragment).commit()
                    }

                    showAuthenticationWithMessage("Logout successful!")
                }
    }

    private fun goToSecuredSection(firebaseUser: FirebaseUser) {
        loggedInUser = DynamicUser(
                firebaseUser.email ?: "Email not available",
                firebaseUser.displayName ?: "Name not available")

        supportFragmentManager.beginTransaction()
                .replace(R.id.container_signed_in_fragment, SignedInFragment.newInstance(loggedInUser!!, eyeTrackerFragment!!))
                .commit()
    }

    private fun showAuthenticationWithMessage(message: String = "") {
        supportFragmentManager.beginTransaction()
                .replace(R.id.container_signed_in_fragment, AuthenticationFragment.newInstance(message), AuthenticationFragment.TAG)
                .commit()
    }

    private fun detachAuthenticationFragment() {
        val authenticationFragment = supportFragmentManager.findFragmentByTag(AuthenticationFragment.TAG)
        authenticationFragment?.let {
            supportFragmentManager.beginTransaction().remove(authenticationFragment).commit()
        }
    }

    private fun initiateEyeTracking() {
        if (supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) == null) {
            eyeTrackerFragment = EyeTrackerFragment.Builder
                    .withFps(20.0f)
                    .withPreview(false)
                    .withPrecision(EyeTrackingLevel.HIGH_PRECISION)
                    .withAwsAccessKeys(
                            PropertyReader.getProperty(PropertyReader.ACCESS_KEY_ID, this),
                            PropertyReader.getProperty(PropertyReader.SECRET_ACCESS_KEY, this))
                    .build()

            supportFragmentManager.inTransaction {
                add(R.id.container_eye_tracker, eyeTrackerFragment, EyeTrackerFragment.TAG)
            }
        } else {
            eyeTrackerFragment = supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) as EyeTrackerFragment
        }
    }

    //@OnFaceAppeared
    fun toCall(eyeContext: EyeTrackerContext) {
        runOnUiThread { toast("OnFaceAppeared") }
    }

    private fun startDynamicAuthenticationControl(username: String) {
        eyeTrackerFragment?.registerUser(username, this)
    }

    override fun onPictureTaken(image: Bitmap?, result: TakePictureResult) {
        toast("User photo associated!")
    }

}
