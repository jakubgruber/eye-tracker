package com.eyetracker.android.demo.authentication.dynamic

import android.databinding.DataBindingUtil
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.communication.picture.PictureTakenListener
import com.eyetracker.android.communication.picture.TakePictureResult
import com.eyetracker.android.communication.verification.UserVerificationListener
import com.eyetracker.android.communication.verification.UserVerificationResult
import me.jakub.dynamicauthentication.R
import me.jakub.dynamicauthentication.databinding.FragmentSignedInBinding
import me.jakub.dynamicauthentication.domain.DynamicUser
import org.jetbrains.anko.support.v4.onUiThread
import org.jetbrains.anko.support.v4.toast

class SignedInFragment : Fragment() {

    var user: DynamicUser? = null
    var eyeTrackerFragment: EyeTrackerFragment? = null

    private val VERIFICATION_INTERVAL = 10000L
    private var lastVerifiedTimeMillis = 0L
    private var handler: Handler? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = DataBindingUtil.inflate<FragmentSignedInBinding>(inflater, R.layout.fragment_signed_in, container, false)
        binding.user = user

        return binding.root
    }

    override fun onResume() {
        super.onResume()

        eyeTrackerFragment?.registerUser(user!!.email, object : PictureTakenListener {
            override fun onPictureTaken(image: Bitmap?, result: TakePictureResult) {
                when (result) {
                    TakePictureResult.OK -> {
                        toastOnUI("Fragment - User photo associated!")

                        startValidation()
                    }
                    else -> toastOnUI("Picture was not associated successfully. Security is reduced.")
                }
            }
        })
    }

    private fun startValidation() {
        val currentTimeMillis = System.currentTimeMillis()

        val timeSinceValidationMillis = currentTimeMillis - lastVerifiedTimeMillis

        if (timeSinceValidationMillis > VERIFICATION_INTERVAL) {
            eyeTrackerFragment?.verifyUser(user!!.email, object : UserVerificationListener {
                override fun onUserVerified(result: UserVerificationResult) {
                    lastVerifiedTimeMillis = System.currentTimeMillis()

                    when (result) {
                        UserVerificationResult.VERIFIED -> toastOnUI("User Verified")
                        else -> toastOnUI("NOT VERIFIED.")
                    }

                    scheduleValidation(VERIFICATION_INTERVAL)
                }
            })
        } else {
            Handler().postDelayed({
                startValidation()
            }, 1)
        }
    }

    private fun scheduleValidation(runAfterMillis: Long) {
        handler = Handler(Looper.getMainLooper())

        handler!!.postDelayed({
            startValidation()
        }, runAfterMillis)
    }

    private fun toastOnUI(message: String) = onUiThread { toast(message) }

    override fun onPause() {
        super.onPause()

        handler?.removeCallbacksAndMessages(null)
    }

    companion object {
        const val TAG = "SignedInFragment"

        fun newInstance(user: DynamicUser, eyeTrackerFragment: EyeTrackerFragment): SignedInFragment {
            val fragment = SignedInFragment()
            fragment.user = user
            fragment.eyeTrackerFragment = eyeTrackerFragment
            return fragment
        }
    }
}
