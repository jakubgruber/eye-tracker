package me.jakub.dynamicauthentication.domain

/**
 * Created by jakub on 04.03.2018.
 */
data class DynamicUser(val email: String, val name: String)