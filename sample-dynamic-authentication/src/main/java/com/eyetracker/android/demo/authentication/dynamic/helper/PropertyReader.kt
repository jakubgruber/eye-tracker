package me.jakub.dynamicauthentication.helper

import android.content.Context
import me.jakub.dynamicauthentication.R
import java.util.*

/**
 * Created by jakub on 08.03.2018.
 */
object PropertyReader {

    val ACCESS_KEY_ID = "aws.access.key.id"
    val SECRET_ACCESS_KEY = "aws.secret.access.key"

    private val properties: Properties = Properties()
    private var propertiesLoaded = false

    fun getProperty(key: String, context: Context): String {
        if (!propertiesLoaded) {
            val inputStream = context.resources.openRawResource(R.raw.aws)
            properties.load(inputStream)
            propertiesLoaded = true
        }

        return properties.getProperty(key)
    }

}