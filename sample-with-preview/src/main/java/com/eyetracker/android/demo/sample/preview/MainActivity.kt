package com.eyetracker.android.demo.sample.preview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.eyetracker.android.EyeTracker
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.annotation.OnYawnDetected
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.eyetracker.android.extension.inTransaction
import me.jakub.samplewithprevirew.R
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    private var eyeTrackerFragment: EyeTrackerFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        EyeTracker.init(this)
        EyeTrackerAnnotationProcessor.inject(this)

        if (supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) == null) {
            eyeTrackerFragment = EyeTrackerFragment.Builder
                    .withFps(20.0f)
                    .withPreview(true)
                    .withPrecision(EyeTrackingLevel.HIGH_PRECISION)
                    .build()

            supportFragmentManager.inTransaction {
                add(R.id.fragment_container, eyeTrackerFragment, EyeTrackerFragment.TAG)
            }
        } else {
            eyeTrackerFragment = supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) as EyeTrackerFragment
        }
    }

    @OnYawnDetected
    fun yawnDetected(eyeTrackerContext: EyeTrackerContext) {
        runOnUiThread {
            toast("Yawn detected")
        }
    }

}
