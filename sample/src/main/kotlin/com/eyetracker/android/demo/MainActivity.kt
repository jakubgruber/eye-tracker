package com.eyetracker.android.demo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.eyetracker.android.EyeTracker
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.configuration.EyeTrackingLevel
import com.eyetracker.android.demo.fragment.ColorChangingFragment
import com.eyetracker.android.demo.fragment.ContentHidingFragment
import com.eyetracker.android.demo.fragment.LoginScreenFragment
import com.eyetracker.android.demo.fragment.VideoFragment
import com.eyetracker.android.extension.inTransaction
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk25.coroutines.onClick

class MainActivity : AppCompatActivity(), AnkoLogger {

    private var eyeTrackerFragment: EyeTrackerFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        EyeTracker.init(this)

        if (supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) == null) {
            eyeTrackerFragment = EyeTrackerFragment.Builder
                    .withFps(20.0f)
                    .withPreview(false)
                    .withPrecision(EyeTrackingLevel.HIGH_PRECISION)
                    .withAwsAccessKeys(
                            PropertyReader.getProperty(PropertyReader.ACCESS_KEY_ID, this),
                            PropertyReader.getProperty(PropertyReader.SECRET_ACCESS_KEY, this))
                    .build()

            supportFragmentManager.inTransaction {
                add(R.id.fragmentContainerEyeTracker, eyeTrackerFragment, EyeTrackerFragment.TAG)
            }
        } else {
            eyeTrackerFragment = supportFragmentManager.findFragmentByTag(EyeTrackerFragment.TAG) as EyeTrackerFragment
        }

        createUI()
    }

    override fun onDestroy() {
        super.onDestroy()

        val fragmentToRemove = supportFragmentManager.findFragmentById(R.id.fragmentContainerSamples)
        if (fragmentToRemove != null) {

            EyeTrackerAnnotationProcessor.disconnect(fragmentToRemove)
        }
    }

    private fun isSampleRunning() = supportFragmentManager.findFragmentById(R.id.fragmentContainerSamples) != null

    private fun removeSampleFragment() {
        if (isSampleRunning()) {
            supportFragmentManager.inTransaction {
                val fragmentToRemove = supportFragmentManager.findFragmentById(R.id.fragmentContainerSamples)

                fragmentToRemove?.let {
                    supportFragmentManager.inTransaction { remove(fragmentToRemove) }
                }
            }
        }
    }

    private fun createUI() {
        startTestingButton.onClick {
            explanationConstraintLayout.visibility = View.GONE
            contentConstraintLayout.visibility = View.VISIBLE

            // start video sample
            val videoFragment = VideoFragment()
            videoFragment.apply {
                removeSampleFragment()
                supportFragmentManager.inTransaction {
                    add(R.id.fragmentContainerSamples, videoFragment)
                }
            }
        }


        bottomNavigation?.setOnNavigationItemSelectedListener(
                { item ->

                    var newFragment: Fragment? = null
                    when (item.itemId) {
                        R.id.action_video -> {
                            newFragment = VideoFragment()
                        }
                        R.id.action_enlarging -> {
                            newFragment = ColorChangingFragment()
                        }
                        R.id.action_secret_content -> {
                            newFragment = ContentHidingFragment()
                        }
                        R.id.action_login -> {
                            newFragment = LoginScreenFragment.newInstance(eyeTrackerFragment!!)
                        }
                        R.id.action_close -> {
                            removeSampleFragment()
                            explanationConstraintLayout.visibility = View.VISIBLE
                            contentConstraintLayout.visibility = View.GONE
                        }
                    }

                    newFragment?.let {
                        removeSampleFragment()
                        supportFragmentManager.inTransaction {
                            add(R.id.fragmentContainerSamples, newFragment)
                        }
                    }

                    true
                })
    }

}
