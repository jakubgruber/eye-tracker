package com.eyetracker.android.demo.fragment

import android.support.v4.app.Fragment

/**
 * Created by jakub on 02.12.2017.
 *
 * Fragment switches to another activity when certain conditions are met
 */
class ActivitySwitcherFragment: Fragment()