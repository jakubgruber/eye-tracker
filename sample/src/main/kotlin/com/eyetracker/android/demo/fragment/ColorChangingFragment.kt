package com.eyetracker.android.demo.fragment

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eyetracker.android.annotation.OnFaceAppeared
import com.eyetracker.android.annotation.OnFaceLost
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.communication.event.AwareComponent
import com.eyetracker.android.communication.event.UserAttentionAwareComponent
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.demo.R
import com.eyetracker.android.extension.inflate
import org.jetbrains.anko.alert
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.okButton

/**
 * Created by jakub on 06.12.2017.
 */
class ColorChangingFragment : Fragment(), UserAttentionAwareComponent {

    private lateinit var articleTextView: TextView
    private var originalColor: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.fragment_color_changing)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        articleTextView = view.findViewById<View>(R.id.text_view_article) as TextView
        originalColor = articleTextView.currentTextColor

        activity!!.alert(Appcompat) {
            title = "Attention Regaining Example"
            message = "Current example shows how the framework can be used to regain user's attention. " +
                    "It changes color of the text when user is not looking to attract his attention back to app. " +
                    "Move your head around, place phone on table to test how it works."
            okButton {
                EyeTrackerAnnotationProcessor.inject(this@ColorChangingFragment)
            }
        }.build().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EyeTrackerAnnotationProcessor.disconnect(this)
    }

    @OnFaceAppeared
    fun onFaceAppeared(eyeContext: EyeTrackerContext) {
        activity!!.runOnUiThread {
            articleTextView.setTextColor(originalColor)
        }
    }

    @OnFaceLost
    fun onFaceLost(eyeContext: EyeTrackerContext) {
        activity!!.runOnUiThread {
            articleTextView.setTextColor(Color.RED)
        }
    }

    override fun awareComponent() = AwareComponent("ColorChangingFragment")

}