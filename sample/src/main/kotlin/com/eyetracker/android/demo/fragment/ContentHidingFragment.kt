package com.eyetracker.android.demo.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eyetracker.android.annotation.OnFaceAppeared
import com.eyetracker.android.annotation.OnFaceLost
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.communication.event.AwareComponent
import com.eyetracker.android.communication.event.UserAttentionAwareComponent
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.demo.R
import com.eyetracker.android.extension.inflate
import kotlinx.android.synthetic.main.fragment_content_hiding.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import java.util.*


/**
 * Created by jakub on 06.12.2017.
 */
class ContentHidingFragment : Fragment(), UserAttentionAwareComponent {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.fragment_content_hiding)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addMoneyFab.onClick {
            val oldValue = accountBalanceTextView.text.replace(Regex("\\$"), "")
            accountBalanceTextView.text = "${oldValue.toInt() + Random().nextInt()}$"
        }

        activity!!.alert(Appcompat) {
            title = "Automatic Secret Hiding Testing"
            message = "This screen shows how an app could use the framework to automatically hide secret section." +
                    " Bank account balance is hidden whenever you are not looking at the screen."
            okButton {
                EyeTrackerAnnotationProcessor.inject(this@ContentHidingFragment)
            }
        }.build().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        EyeTrackerAnnotationProcessor.disconnect(this)
    }

    @OnFaceAppeared
    fun showSecret(eyeContext: EyeTrackerContext) {
        activity!!.runOnUiThread {
            accountBalanceTextView.visibility = View.VISIBLE
        }
    }

    @OnFaceLost
    fun hideSecret(eyeContext: EyeTrackerContext) {
        activity!!.runOnUiThread {
            accountBalanceTextView.visibility = View.INVISIBLE
        }
    }

    override fun awareComponent() = AwareComponent("ContentHidingFragment")

}