package com.eyetracker.android.demo.fragment

import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eyetracker.android.EyeTrackerFragment
import com.eyetracker.android.communication.picture.PictureTakenListener
import com.eyetracker.android.communication.picture.TakePictureResult
import com.eyetracker.android.communication.verification.UserVerificationListener
import com.eyetracker.android.communication.verification.UserVerificationResult
import com.eyetracker.android.demo.R
import com.eyetracker.android.extension.inflate
import kotlinx.android.synthetic.main.fragment_login_screen.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.longToast

/**
 * Created by jakub on 17.12.2017.
 */
class LoginScreenFragment : Fragment(), PictureTakenListener, UserVerificationListener {

    var eyeTracker: EyeTrackerFragment? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.fragment_login_screen)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerButton
                .apply { isEnabled = false }
                .onClick {
                    registrationProgressBar.visibility = View.VISIBLE
                    eyeTracker?.registerUser(usernameEditText.text.toString(), this@LoginScreenFragment)
                }

        verifyButton
                .apply { isEnabled = false }
                .onClick {
                    userStatusImageView.imageResource = R.mipmap.ic_user
                    registrationProgressBar.visibility = View.VISIBLE
                    eyeTracker?.verifyUser(usernameEditText.text.toString(), this@LoginScreenFragment)
                }

        activity!!.alert(Appcompat) {
            title = "Dynamic Authentication Testing"
            message = "Following screen shows usage of the framework for dynamic authentication." +
                    " Choose your username, register into app by clicking on register button and then " +
                    "click on verify button. Icon will change to indicate success or failure. Please, test " +
                    "accuracy and overall success rate of the framework. " +
                    "You can register a new photo just by click register button again."
            okButton {
                registerButton.isEnabled = true
                verifyButton.isEnabled = true
            }
        }.build().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }

    override fun onPictureTaken(image: Bitmap?, result: TakePictureResult) {
        registrationProgressBar.visibility = View.GONE
        when (result) {
            TakePictureResult.OK -> {
                imageViewFace.visibility = View.VISIBLE
                imageViewFace.setImageBitmap(image)

                verifyButton.isEnabled = true

                verifyButton.visibility = View.GONE
                registerButton.visibility = View.GONE

                Handler().postDelayed({
                    imageViewFace.visibility = View.GONE

                    verifyButton.visibility = View.VISIBLE
                    registerButton.visibility = View.VISIBLE
                }, 1500)
            }
            TakePictureResult.NULL_USERNAME -> {
                longToast("Null username")
            }
            TakePictureResult.UNEXPECTED_ERROR -> {
                longToast("Unexpected error")
            }
        }
    }

    override fun onUserVerified(result: UserVerificationResult) {
        activity!!.runOnUiThread {
            registrationProgressBar.visibility = View.GONE
            when (result) {
                UserVerificationResult.VERIFIED -> {
                    userStatusImageView.imageResource = R.mipmap.ic_verified_user_round
                }
                else -> {
                    userStatusImageView.imageResource = R.mipmap.ic_user_unknown_round
                }
            }
        }
    }

    companion object {

        fun newInstance(eyeTrackerFragment: EyeTrackerFragment): LoginScreenFragment {
            val instance = LoginScreenFragment()
            instance.eyeTracker = eyeTrackerFragment
            return instance
        }

    }

}