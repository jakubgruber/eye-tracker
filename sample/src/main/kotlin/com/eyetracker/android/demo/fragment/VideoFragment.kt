package com.eyetracker.android.demo.fragment

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import com.eyetracker.android.annotation.OnContextUpdate
import com.eyetracker.android.annotation.OnFaceAppeared
import com.eyetracker.android.annotation.OnFaceLost
import com.eyetracker.android.annotation.processor.EyeTrackerAnnotationProcessor
import com.eyetracker.android.communication.event.AwareComponent
import com.eyetracker.android.communication.event.UserAttentionAwareComponent
import com.eyetracker.android.communication.tracker.EyeTrackerContext
import com.eyetracker.android.demo.R
import com.eyetracker.android.extension.inflate
import kotlinx.android.synthetic.main.fragment_video.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.appcompat.v7.Appcompat
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by jakub on 02.12.2017.
 *
 * Fragment turn on and off playing video when user loses/gains attention
 */
class VideoFragment : Fragment(), UserAttentionAwareComponent {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            container?.inflate(R.layout.fragment_video)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mediaController = MediaController(context)
        mediaController.setAnchorView(view)

        val filePath = "android.resource://${context!!.packageName}/${R.raw.sample_video}"
        val uri = Uri.parse(filePath)

        videoView.apply {
            setMediaController(mediaController)
            setVideoURI(uri)
            setOnPreparedListener { mediaPlayer -> mediaPlayer.isLooping = true }
            requestFocus()
            onClick { stopOrResumeVideo() }
        }

        activity!!.alert(Appcompat) {
            title = "Video Testing"
            message = "Eye Tracker Framework allows developer to take advantage of eye tracking in an app. " +
                    "This simple sample demonstrates how to control video by eye and head movements. " +
                    "Try to control the video by blinking, closing eyes and looking away and back again."
            okButton {
                EyeTrackerAnnotationProcessor.inject(this@VideoFragment)
            }
        }.build().apply {
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }.show()
    }

    override fun onResume() {
        super.onResume()
        videoView.start()
    }

    override fun onPause() {
        super.onPause()
        videoView.pause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        videoView.stopPlayback()
        EyeTrackerAnnotationProcessor.disconnect(this)
    }

    @OnFaceAppeared
    fun startVideo(eyeContext: EyeTrackerContext) {
        activity!!.runOnUiThread {
            if (!videoView.isPlaying) {
                videoView.start()
            }
        }
    }

    @OnFaceLost
    fun stopVideo(eyeContext: EyeTrackerContext) {
        if (videoView.isPlaying) {
            videoView.pause()
        }
    }

    @OnContextUpdate
    fun reactOnEyeBlink(eyeContext: EyeTrackerContext) {
        if (eyeContext.blinkDetected) {
            activity!!.runOnUiThread {
                stopOrResumeVideo()
            }
        }
    }

    private fun stopOrResumeVideo() {
        if (videoView.isPlaying) {
            videoView.pause()
        } else {
            videoView.start()
        }
    }

    override fun awareComponent() = AwareComponent("VideoFragment")

}